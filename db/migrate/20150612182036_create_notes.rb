class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :title
      t.text :description, limit: 140
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :notes, :users
  end
end
